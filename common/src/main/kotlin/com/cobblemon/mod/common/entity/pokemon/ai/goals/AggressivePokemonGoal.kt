/*
 * Copyright (C) 2023 Cobblemon Contributors
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */


package com.cobblemon.mod.common.entity.pokemon

import com.cobblemon.mod.common.api.types.ElementalType
import com.cobblemon.mod.common.api.types.ElementalTypes
import net.minecraft.block.Block
import net.minecraft.block.BlockState
import net.minecraft.block.Blocks
import net.minecraft.enchantment.Enchantments
import net.minecraft.entity.AreaEffectCloudEntity
import net.minecraft.entity.Entity
import net.minecraft.entity.LivingEntity
import net.minecraft.entity.ai.goal.Goal
import net.minecraft.entity.attribute.EntityAttributes
import net.minecraft.entity.effect.StatusEffect
import net.minecraft.entity.effect.StatusEffectInstance
import net.minecraft.entity.effect.StatusEffects
import net.minecraft.entity.player.PlayerEntity
import net.minecraft.entity.projectile.*
import net.minecraft.item.ItemStack
import net.minecraft.item.Items
import net.minecraft.loot.context.LootContextParameterSet
import net.minecraft.loot.context.LootContextParameters
import net.minecraft.particle.DefaultParticleType
import net.minecraft.particle.ParticleTypes
import net.minecraft.registry.tag.BlockTags
import net.minecraft.registry.tag.TagKey
import net.minecraft.server.world.ServerWorld
import net.minecraft.sound.SoundEvents
import net.minecraft.util.hit.EntityHitResult
import net.minecraft.util.math.BlockPos
import net.minecraft.util.math.Box
import net.minecraft.util.math.MathHelper
import net.minecraft.util.math.Vec3d
import net.minecraft.world.GameRules
import net.minecraft.world.World
import net.minecraft.world.event.GameEvent
import java.util.*


internal class CustomSmallFireballEntity(world:World,  owner:LivingEntity,  velocityX:Double,  velocityY:Double,  velocityZ:Double,private val damageAmount:Float = 5.0f):
    SmallFireballEntity(world, owner, velocityX, velocityY, velocityZ){
    override protected fun onEntityHit(entityHitResult: EntityHitResult) {
        super.onEntityHit(entityHitResult)
        if (!this.getWorld().isClient) {
            val entity = entityHitResult.entity
            val entity2: Entity? = this.getOwner()
            val i = entity.fireTicks
            entity.setOnFireFor(5)
            if (!entity.damage(this.getDamageSources().fireball(this, entity2), damageAmount)) {
                entity.fireTicks = i
            } else if (entity2 is LivingEntity) {
                this.applyDamageEffects(entity2, entity)
            }
        }
    }
}

internal class CustomFireBeamGoal(private val pokemon: PokemonEntity) : Goal() {
    private var beamTicks = 0
    private val elder: Boolean
    private var skillParticle : DefaultParticleType = ParticleTypes.NOTE
    private var skillParticleList= mutableListOf<DefaultParticleType>(ParticleTypes.NOTE)
    init {
        elder = false//guardian is ElderGuardianEntity
        controls =
            EnumSet.of(Control.MOVE, Control.LOOK)

    }

    fun changeType(type: ElementalType?){
        when(type){
            ElementalTypes.NORMAL->{skillParticleList.addAll(mutableListOf<DefaultParticleType>(ParticleTypes.SWEEP_ATTACK))}
            ElementalTypes.FIGHTING->{skillParticleList.addAll(mutableListOf<DefaultParticleType>(ParticleTypes.CRIT))}
            ElementalTypes.FLYING->{skillParticleList.addAll(mutableListOf<DefaultParticleType>(ParticleTypes.CLOUD))}
            ElementalTypes.POISON->{skillParticleList.addAll(mutableListOf<DefaultParticleType>(ParticleTypes.PORTAL))}
            ElementalTypes.GROUND->{skillParticleList.addAll(mutableListOf<DefaultParticleType>(ParticleTypes.ITEM_SLIME))}
            ElementalTypes.ROCK->{skillParticleList.addAll(mutableListOf<DefaultParticleType>(ParticleTypes.DRIPPING_HONEY))}
            ElementalTypes.BUG->{skillParticleList.addAll(mutableListOf<DefaultParticleType>(ParticleTypes.NAUTILUS))}
            ElementalTypes.GHOST->{skillParticleList.addAll(mutableListOf<DefaultParticleType>(ParticleTypes.SOUL_FIRE_FLAME))}
            ElementalTypes.STEEL->{skillParticleList.addAll(mutableListOf<DefaultParticleType>(ParticleTypes.SONIC_BOOM))}
            ElementalTypes.FIRE->{skillParticleList.addAll(mutableListOf<DefaultParticleType>(ParticleTypes.SMALL_FLAME))}
            ElementalTypes.WATER->{skillParticleList.addAll(mutableListOf<DefaultParticleType>(ParticleTypes.BUBBLE))}
            ElementalTypes.GRASS->{skillParticleList.addAll(mutableListOf<DefaultParticleType>(ParticleTypes.COMPOSTER))}
            ElementalTypes.ELECTRIC->{skillParticleList.addAll(mutableListOf<DefaultParticleType>(ParticleTypes.ELECTRIC_SPARK))}
            ElementalTypes.PSYCHIC->{skillParticleList.addAll(mutableListOf<DefaultParticleType>(ParticleTypes.ENTITY_EFFECT))}
            ElementalTypes.ICE->{skillParticleList.addAll(mutableListOf<DefaultParticleType>(ParticleTypes.ITEM_SNOWBALL,ParticleTypes.SNOWFLAKE))}
            ElementalTypes.DRAGON->{skillParticleList.addAll(mutableListOf<DefaultParticleType>(ParticleTypes.DRAGON_BREATH))}
            ElementalTypes.DARK->{skillParticleList.addAll(mutableListOf<DefaultParticleType>(ParticleTypes.LARGE_SMOKE,ParticleTypes.SQUID_INK))}
            ElementalTypes.FAIRY->{skillParticleList.addAll(mutableListOf<DefaultParticleType>(ParticleTypes.HEART,ParticleTypes.CHERRY_LEAVES))}
            else ->{}
        }
    }

    override fun canStart(): Boolean {
        val livingEntity = pokemon.target
        return livingEntity != null && livingEntity.isAlive && !pokemon.isBattling
    }

    override fun shouldContinue(): Boolean {
        return super.shouldContinue() && (elder || pokemon.target != null && pokemon.squaredDistanceTo(pokemon.target) > 9.0)
    }

    override fun start() {
        beamTicks = 0
        //pokemon.navigation.stop()
        skillParticleList.clear()
        changeType(pokemon.pokemon.primaryType)
        changeType(pokemon.pokemon.secondaryType)
        val livingEntity = pokemon.target
        if (livingEntity != null) {
            pokemon.lookControl.lookAt(livingEntity, 90.0f, 90.0f)
        }
        //pokemon.velocityDirty = true
    }

    override fun stop() {
        //guardian.setBeamTarget(0)
        //guardian.target = null as LivingEntity?
        //guardian.wanderGoal!!.ignoreChanceOnce()
    }

    override fun shouldRunEveryTick(): Boolean {
        return true
    }

    override fun tick() {
        val livingEntity = pokemon.target
        if (livingEntity != null) {
            //pokemon.navigation.stop()
            pokemon.lookControl.lookAt(livingEntity, 90.0f, 90.0f)
            if (!pokemon.canSee(livingEntity)) {
                //pokemon.target = null as LivingEntity?
                pokemon.moveControl.moveTo(livingEntity.x, livingEntity.y, livingEntity.z, pokemon.getAttributeInstance(EntityAttributes.GENERIC_MOVEMENT_SPEED)!!.baseValue)
            } else {
                ++beamTicks
                if(beamTicks%30 ==0 && pokemon.random.nextBoolean())//beamTicks >= pokemon.WARMUP_TIME &&
                if (pokemon.isInRange(livingEntity, 10.0, 20.0)) {
                    livingEntity.damage(pokemon.damageSources.indirectMagic(pokemon, pokemon), pokemon.pokemon.specialAttack.toFloat()/25.0f)
//                    livingEntity.damage(
//                        pokemon.damageSources.mobAttack(pokemon), pokemon.pokemon.specialAttack.toFloat()/25.0f//pokemon.getAttributeValue(EntityAttributes.GENERIC_ATTACK_DAMAGE).toFloat()
//                    )
                            val vec3d: Vec3d = pokemon.getEyePos()//.add(0.0, 1.600000023841858, 0.0)
                            val vec3d2: Vec3d = livingEntity.getPos().subtract(vec3d)
                            val vec3d3 = vec3d2.normalize()
                            skillParticle = skillParticleList.random()
                            for (i in 1..MathHelper.floor(vec3d2.length()) + 7 step 1) {
                                val vec3d4 = vec3d.add(vec3d3.multiply(i.toDouble()))
                                for(ii in 1..4)
                                (pokemon.getWorld() as ServerWorld).spawnParticles<DefaultParticleType>(
                                    skillParticle,
                                    vec3d4.x+pokemon.random.nextDouble(),
                                    vec3d4.y+pokemon.random.nextDouble(),
                                    vec3d4.z+pokemon.random.nextDouble(),
                                    1,
                                    0.0,
                                    0.0,
                                    0.0,
                                    0.0
                                )
                            }

                    //guardian.target = null as LivingEntity?
                }else{
                    pokemon.moveControl.moveTo(livingEntity.x, livingEntity.y, livingEntity.z, pokemon.getAttributeInstance(EntityAttributes.GENERIC_MOVEMENT_SPEED)!!.baseValue)

                }

            }
            super.tick()
        }
    }
}




public class PokemonTeleportGoal(private val pokemon: PokemonEntity) : Goal() {
    var cooldown = 0

    init{
    }
    override fun canStart(): Boolean {
        return !pokemon.isBattling
    }
    override fun start() {
        cooldown = 0
    }
    override fun stop() {
        pokemon.setShooting(false)
    }
    override fun shouldRunEveryTick(): Boolean {
        return true
    }
    override fun tick() {

        val livingEntity = pokemon.target
        if (livingEntity != null) {
            val d = 64.0
            if (livingEntity.squaredDistanceTo(pokemon) < 4096.0) {
                val world = pokemon.world
                ++cooldown
                if (cooldown == 10 && !pokemon.isSilent) {
                    //world.syncWorldEvent(null as PlayerEntity?, 1015, ghast.blockPos, 0)
                }
                if (cooldown == 40) {
                    pokemon.teleportTo(livingEntity)
                    cooldown = -60
                }
            } else if (cooldown > 0) {
                --cooldown
            }

        }else{
            if(pokemon.random.nextDouble()<0.02 && !pokemon.isBattling){
                pokemon.teleportRandomly()
            }
        }
    }
}


class BuffInfo(public val particle:DefaultParticleType?,
               public val duration:Int,public val frequency:Int, public val radius:Float,public val effect: StatusEffect?,public val agro: Boolean = false,public val support: Boolean = false
){
    fun castEffect(pokemon:PokemonEntity){
        var target = pokemon as LivingEntity
        if(agro) {
            if (pokemon.target != null)
                if(pokemon.canSee(pokemon.target))
                    target = pokemon.target as LivingEntity
            else
                return
            else
                return
        }
        if(support){
            if(pokemon.owner as? LivingEntity != null)
                target = pokemon.owner as LivingEntity
            else
                return
        }
        if(particle!= null) {
            val areaEffectCloudEntity = AreaEffectCloudEntity(
                target.getWorld(),
                target.getX(),
                target.getY(),
                target.getZ()
            )
            areaEffectCloudEntity.owner = pokemon
            areaEffectCloudEntity.particleType = particle
            areaEffectCloudEntity.radius = radius
            areaEffectCloudEntity.duration = duration
            areaEffectCloudEntity.radiusGrowth =
                (7.0f - areaEffectCloudEntity.radius) / areaEffectCloudEntity.duration.toFloat()
            if(effect != null)
                areaEffectCloudEntity.addEffect(StatusEffectInstance(effect, 1, 0))
            //areaEffectCloudEntity.setPosition(livingEntity.getX(), livingEntity.getY(), livingEntity.getZ())
            target.getWorld().spawnEntity(areaEffectCloudEntity)
        }else{
            target.addStatusEffect(StatusEffectInstance(effect, duration, 0), target)
        }
    }
}

public class PokemonSelfCastGoal(private val pokemon: PokemonEntity) : Goal() {
    var cooldown = 0


    private var buffInfoList= mutableListOf<BuffInfo>()
    init{
        addBuff(pokemon.pokemon.primaryType,pokemon)
        addBuff(pokemon.pokemon.secondaryType,pokemon)
    }
    fun addBuff(type: ElementalType?,target:LivingEntity){
        when(type){
            ElementalTypes.NORMAL->{}
            ElementalTypes.FIGHTING->{buffInfoList.addAll(mutableListOf<BuffInfo>(
                BuffInfo(null,60,100,0.0f,StatusEffects.STRENGTH),
                BuffInfo(null,600,700,1.0f, StatusEffects.STRENGTH,false,true)))}
            ElementalTypes.FLYING->{buffInfoList.addAll(mutableListOf<BuffInfo>(
                BuffInfo(null,600,700,1.0f, StatusEffects.JUMP_BOOST,false,true)))}
            ElementalTypes.POISON->{}
            ElementalTypes.GROUND->{buffInfoList.addAll(mutableListOf<BuffInfo>(
                BuffInfo(null,60,100,0.0f,StatusEffects.ABSORPTION),
                BuffInfo(null,600,700,1.0f, StatusEffects.ABSORPTION,false,true)))}
            ElementalTypes.ROCK->{buffInfoList.addAll(mutableListOf<BuffInfo>(
                BuffInfo(null,60,100,0.0f,StatusEffects.HEALTH_BOOST),
                BuffInfo(null,600,700,1.0f, StatusEffects.HEALTH_BOOST,false,true)))}
            ElementalTypes.BUG->{buffInfoList.addAll(mutableListOf<BuffInfo>(
                BuffInfo(null,100,100,1.0f, StatusEffects.GLOWING,true)))}
            ElementalTypes.GHOST->{buffInfoList.addAll(mutableListOf<BuffInfo>(
                BuffInfo(null,60,100,0.0f,StatusEffects.INVISIBILITY)))}
            ElementalTypes.STEEL->{buffInfoList.addAll(mutableListOf<BuffInfo>(
                BuffInfo(null,60,100,0.0f,StatusEffects.RESISTANCE),
                BuffInfo(null,600,700,1.0f, StatusEffects.RESISTANCE,false,true)))}
            ElementalTypes.FIRE->{buffInfoList.addAll(mutableListOf<BuffInfo>(
                BuffInfo(null,600,700,1.0f, StatusEffects.FIRE_RESISTANCE,false,true)))}
            ElementalTypes.WATER->{buffInfoList.addAll(mutableListOf<BuffInfo>(
                BuffInfo(null,600,700,0.0f,StatusEffects.WATER_BREATHING,false,true),
                BuffInfo(null,600,700,0.0f,StatusEffects.DOLPHINS_GRACE,false,true)))}
            ElementalTypes.GRASS->{buffInfoList.addAll(mutableListOf<BuffInfo>(
                BuffInfo(null,60,150,0.0f,StatusEffects.REGENERATION),
                BuffInfo(ParticleTypes.COMPOSTER,60,150,2.0f,StatusEffects.INSTANT_HEALTH)))}
            ElementalTypes.ELECTRIC->{buffInfoList.addAll(mutableListOf<BuffInfo>(
                BuffInfo(null,40,100,0.0f,StatusEffects.HASTE),
                BuffInfo(ParticleTypes.ELECTRIC_SPARK,30,150,1.0f, StatusEffects.INSTANT_DAMAGE,true),
                BuffInfo(null,600,700,1.0f, StatusEffects.SPEED,false,true)))}
            ElementalTypes.PSYCHIC->{}
            ElementalTypes.ICE->{}
            ElementalTypes.DRAGON->{}
            ElementalTypes.DARK->{buffInfoList.addAll(mutableListOf<BuffInfo>(
                BuffInfo(null,600,700,1.0f, StatusEffects.NIGHT_VISION,false,true)))}
            ElementalTypes.FAIRY->{
                buffInfoList.addAll(mutableListOf<BuffInfo>(
                    BuffInfo(ParticleTypes.CHERRY_LEAVES,40,150,1.0f, StatusEffects.INSTANT_DAMAGE,true),
                    BuffInfo(null,600,700,1.0f, StatusEffects.LUCK,false,true)))}
            else ->{}
        }
        //LOGGER.info("applyStatusEffect ${type.toString()}")
    }
    override fun canStart(): Boolean {
        return !pokemon.isBattling
    }
    override fun start() {
        cooldown = 0
    }
    override fun stop() {
        pokemon.setShooting(false)
    }
    override fun shouldRunEveryTick(): Boolean {
        return true
    }
    override fun tick() {
        if (true) {
            val d = 64.0
                val world = pokemon.world
                ++cooldown
                if (cooldown == 10) {
                    for(buff:BuffInfo in buffInfoList){
                        if(pokemon.random.nextDouble()< 10.0f/buff.frequency && !pokemon.isBattling){
                            buff.castEffect(pokemon)
                        }
                    }
                    cooldown = 0
                }
        }
    }
}

public class ShootFireballGoalGhast(private val pokemon: PokemonEntity) : Goal() {
    private val ITEM: ItemStack? = null
    var cooldown = 0

    init{
    }
    override fun canStart(): Boolean {
        return pokemon.target != null
    }
    override fun start() {
        cooldown = 0
    }
    override fun stop() {
        pokemon.setShooting(false)
    }
    override fun shouldRunEveryTick(): Boolean {
        return true
    }
    override fun tick() {
        val livingEntity = pokemon.target
        if (livingEntity != null) {
            val d = 64.0
            if (livingEntity.squaredDistanceTo(pokemon) < 4096.0 && pokemon.canSee(livingEntity)) {
                val world = pokemon.world
                ++cooldown
                if (cooldown == 10 && !pokemon.isSilent) {
                    //world.syncWorldEvent(null as PlayerEntity?, 1015, ghast.blockPos, 0)
                }
                if (cooldown == 40) {
                    val e = 4.0
                    val vec3d = pokemon.getRotationVec(1.0f)
                    val f = livingEntity.x - (pokemon.x + vec3d.x * 4.0)
                    val g = livingEntity.getBodyY(0.5) - (0.5 + pokemon.getBodyY(0.5))
                    val h = livingEntity.z - (pokemon.z + vec3d.z * 4.0)
                    if (!pokemon.isSilent) {
                        world.syncWorldEvent(null as PlayerEntity?, 1016, pokemon.blockPos, 0)
                    }

                    if(pokemon.pokemon.primaryType == ElementalTypes.DRAGON || pokemon.pokemon.secondaryType == ElementalTypes.DRAGON){
                        val fireballEntity = DragonFireballEntity(world, pokemon, f, g, h)
                        fireballEntity.setPosition(
                            pokemon.x + vec3d.x * 4.0,
                            pokemon.getBodyY(0.5) + 0.5,
                            fireballEntity.z + vec3d.z * 4.0
                        )
                        fireballEntity.setVelocity(vec3d.x,vec3d.y,vec3d.z,10.0f,0.0f)
                        world.spawnEntity(fireballEntity)
                    }else{
                        var fireballEntity = FireballEntity(world, pokemon, f, g, h,pokemon.pokemon.specialAttack.toInt()/40)
                        fireballEntity.setPosition(
                            pokemon.x + vec3d.x * 4.0,
                            pokemon.getBodyY(0.5) + 0.5,
                            fireballEntity.z + vec3d.z * 4.0
                        )
                        fireballEntity.setVelocity(vec3d.x,vec3d.y,vec3d.z,10.0f,0.0f)
                        world.spawnEntity(fireballEntity)
                    }

                    cooldown = -60
                }
            } else if (cooldown > 0) {
                --cooldown
            }
            pokemon.setShooting(cooldown > 10)
        }
    }
}
public class ShootFireballGoal(private val pokemon: PokemonEntity) : Goal() {
    private var fireballsFired = 0
    private var fireballCooldown = 0
    private var targetNotVisibleTicks = 0

    init {
        controls =
            EnumSet.of(Control.MOVE, Control.LOOK)
    }

    override fun canStart(): Boolean {
        val livingEntity = pokemon.target
        return livingEntity != null && livingEntity.isAlive && pokemon.canTarget(livingEntity)
    }

    override fun start() {
        fireballsFired = 0
    }

    override fun stop() {
        pokemon.setFireActive(false)
        targetNotVisibleTicks = 0
    }

    override fun shouldRunEveryTick(): Boolean {
        return true
    }

    override fun tick() {
        --fireballCooldown
        val livingEntity = pokemon.target
        if (livingEntity != null) {
            val bl = pokemon.visibilityCache.canSee(livingEntity)
            if (bl) {
                targetNotVisibleTicks = 0
            } else {
                ++targetNotVisibleTicks
            }
            val d = pokemon.squaredDistanceTo(livingEntity)
            if (d < 4.0) {
                if (!bl) {
                    return
                }
                if (fireballCooldown <= 0) {
                    fireballCooldown = 20
                    pokemon.tryAttack(livingEntity)
                }
                pokemon.moveControl.moveTo(livingEntity.x, livingEntity.y, livingEntity.z, 1.0)
            } else if (d < followRange * followRange && bl) {
                val e = livingEntity.x - pokemon.x
                val f = livingEntity.getBodyY(0.5) - pokemon.getBodyY(0.5)
                val g = livingEntity.z - pokemon.z
                if (fireballCooldown <= 0) {
                    ++fireballsFired
                    if (fireballsFired == 1) {
                        fireballCooldown = 60
                        pokemon.setFireActive(true)
                    } else if (fireballsFired <= 4) {
                        fireballCooldown = 6
                    } else {
                        fireballCooldown = 100
                        fireballsFired = 0
                        pokemon.setFireActive(false)
                    }
                    if (fireballsFired > 1) {
                        val h = Math.sqrt(Math.sqrt(d)) * 0.5
                        if (!pokemon.isSilent) {
                            pokemon.world.syncWorldEvent(null as PlayerEntity?, 1018, pokemon.blockPos, 0)
                        }
                        for (i in 0..0) {
                            val smallFireballEntity = CustomSmallFireballEntity(
                                pokemon.world,
                                pokemon,
                                pokemon.random.nextTriangular(e, 2.297 * h),
                                f,
                                pokemon.random.nextTriangular(g, 2.297 * h),
                                pokemon.pokemon.specialAttack/20.0f
                            )
                            smallFireballEntity.setPosition(
                                smallFireballEntity.x,
                                pokemon.getBodyY(0.5) + 0.5,
                                smallFireballEntity.z
                            )
                            pokemon.world.spawnEntity(smallFireballEntity)
                        }
                    }
                }
                pokemon.lookControl.lookAt(livingEntity, 10.0f, 10.0f)
            } else if (targetNotVisibleTicks < 5) {
                pokemon.moveControl.moveTo(livingEntity.x, livingEntity.y, livingEntity.z, 1.0)
            }
            super.tick()
        }
    }

    private val followRange: Double
        private get() = pokemon.getAttributeValue(EntityAttributes.GENERIC_FOLLOW_RANGE)
}

internal class PickUpBlockPokemonGoal(private val pokemon: PokemonEntity) : Goal() {
    val listOfCollectolableBlock:Set<TagKey<Block>> =  setOf(BlockTags.NEEDS_STONE_TOOL,BlockTags.NEEDS_IRON_TOOL)
    val listOfDigBlock :Set<TagKey<Block>> =  setOf(BlockTags.ENDERMAN_HOLDABLE,BlockTags.BASE_STONE_OVERWORLD,BlockTags.BASE_STONE_NETHER)
    var intervalTick = 20
    var cooldown = 0
    override fun canStart(): Boolean {
        return if (!pokemon.world.gameRules.getBoolean(GameRules.DO_MOB_GRIEFING)) {
            false
        } else {
             !pokemon.isBattling
        }
    }
    fun findNearestBlock( X:Int):BlockPos?{
        var x=0
        var y=0
        var dx=0
        var dy=-1
        var blockPos: BlockPos? = null
        var t = X
        val maxI = X*X

        var kx = 1
        var ky = 1
        if(pokemon.random.nextBoolean())
            kx=-1
        if(pokemon.random.nextBoolean())
            ky=-1
        for(i in 0..maxI){
            if ((-X/2 <= x) && (x <= X/2) && (-X/2 <= y) && (y <= X/2)){
                for(h in 0..pokemon.height.toInt()+1){
                    blockPos = BlockPos(pokemon.blockX.toInt()+x*kx, pokemon.blockY.toInt()+h, pokemon.blockZ.toInt()+y*ky)
                    if(!pokemon.world.getBlockState(blockPos).isAir() && !pokemon.world.getBlockState(blockPos).isLiquid)
                        return blockPos
                }
            }
            if( (x == y) || ((x < 0) && (x == -y)) || ((x > 0) && (x == 1-y))){
                t = dx;
                dx = -dy;
                dy = t;
            }
            x += dx;
            y += dy;
        }
        return blockPos
    }
    fun findFluidBlock( blockPos:BlockPos):BlockPos? {
        var fluidPos: BlockPos? = null
        if(pokemon.world.getBlockState(BlockPos(blockPos.x, blockPos.y+1, blockPos.z)).isLiquid())
            fluidPos = BlockPos(blockPos.x, blockPos.y+1, blockPos.z)
        else if(pokemon.world.getBlockState(BlockPos(blockPos.x, blockPos.y-1, blockPos.z)).isLiquid())
            fluidPos = BlockPos(blockPos.x, blockPos.y-1, blockPos.z)
        else if(pokemon.world.getBlockState(BlockPos(blockPos.x+1, blockPos.y, blockPos.z)).isLiquid())
            fluidPos = BlockPos(blockPos.x+1, blockPos.y, blockPos.z)
        else if(pokemon.world.getBlockState(BlockPos(blockPos.x-1, blockPos.y, blockPos.z)).isLiquid())
            fluidPos = BlockPos(blockPos.x-1, blockPos.y, blockPos.z)
        else if(pokemon.world.getBlockState(BlockPos(blockPos.x, blockPos.y, blockPos.z+1)).isLiquid())
            fluidPos = BlockPos(blockPos.x, blockPos.y, blockPos.z+1)
        else if(pokemon.world.getBlockState(BlockPos(blockPos.x, blockPos.y, blockPos.z-1)).isLiquid())
            fluidPos = BlockPos(blockPos.x, blockPos.y, blockPos.z-1)
        return fluidPos
    }
    fun dig(){
        val world = pokemon.world
        var bl = false
        var blockPos: BlockPos? = null
        var blockState: BlockState? = null
        var collectable = false
        var diggable = false

        blockPos = findNearestBlock(pokemon.width.toInt()+14)
        if(blockPos == null)
            return
        blockState = world.getBlockState(blockPos)
//        val vec3d = Vec3d(pokemon.blockX.toDouble() + 0.5, blockPos.y.toDouble() + 0.5, pokemon.blockZ.toDouble() + 0.5)
//        val vec3d2 = Vec3d(blockPos.x.toDouble() + 0.5, blockPos.y.toDouble() + 0.5, blockPos.z.toDouble() + 0.5)
//        val blockHitResult = world.raycast(
//                RaycastContext(
//                    vec3d, vec3d2, RaycastContext.ShapeType.OUTLINE, RaycastContext.FluidHandling.NONE,
//                    pokemon
//                )
//            )
        bl = true//blockHitResult.blockPos == blockPos
        if(bl) {
            collectable = listOfCollectolableBlock.stream().anyMatch { blockState!!.isIn(it) }
            diggable = listOfDigBlock.stream().anyMatch { blockState!!.isIn(it) }
        }
        if ((collectable || diggable) && bl) {
            val fluidPos = findFluidBlock(blockPos)
            if(fluidPos != null ){
                if(pokemon.carriedBlock == null)
                    return

                val block = Block.postProcessState(pokemon.carriedBlock, pokemon.world, fluidPos)
                world.setBlockState(fluidPos, block, 3)
                world.emitGameEvent(
                    GameEvent.BLOCK_PLACE, fluidPos, GameEvent.Emitter.of(
                        pokemon, block
                    )
                )
                pokemon.carriedBlock = null
                return
            }
            world.removeBlock(blockPos, false)
            world.emitGameEvent(
                GameEvent.BLOCK_DESTROY, blockPos, GameEvent.Emitter.of(
                    pokemon, blockState
                )
            )
            if(!pokemon.world.getBlockState(BlockPos(blockPos.x,pokemon.y.toInt()-1,blockPos.z)).isAir())
                pokemon.moveControl.moveTo(blockPos.x.toDouble()+ 0.5, pokemon.y, blockPos.z.toDouble()+ 0.5, pokemon.getAttributeInstance(EntityAttributes.GENERIC_MOVEMENT_SPEED)!!.baseValue)
            pokemon.lookControl.lookAt(blockPos.x.toDouble() + 0.5, blockPos.y.toDouble() + 0.5, blockPos.z.toDouble() + 0.5)
            if(true){
                if(pokemon.world as? ServerWorld != null){
                    var skillParticle : DefaultParticleType = ParticleTypes.EXPLOSION
                    (pokemon.world as ServerWorld).spawnParticles<DefaultParticleType>(
                        skillParticle,
                        blockPos.x.toDouble() + 0.5, blockPos.y.toDouble() + 0.5, blockPos.z.toDouble() + 0.5,
                        3,
                        0.0,
                        0.0,
                        0.0,
                        0.0
                    )
                }
            }
            if (world is ServerWorld ) {
                if (diggable) {
                    var blockState3 = blockState!!.block.defaultState
                    if(pokemon.carriedBlock == null)
                        pokemon.carriedBlock = blockState!!.block.defaultState
                    else
                        if (blockState3 != null) {
                            val itemStack = ItemStack(Items.DIAMOND_AXE)
                            itemStack.addEnchantment(Enchantments.SILK_TOUCH, 1)
                            val builder = LootContextParameterSet.Builder(world as ServerWorld?)
                                .add<Vec3d>(LootContextParameters.ORIGIN, pokemon.pos)
                                .add<ItemStack>(LootContextParameters.TOOL, itemStack).addOptional<Entity>(
                                    LootContextParameters.THIS_ENTITY, pokemon
                                )
                            val list = blockState3.getDroppedStacks(builder)
                            val var8: Iterator<*> = list.iterator()
                            while (var8.hasNext()) {
                                val itemStack2 = var8.next() as ItemStack
                                pokemon.dropStack(itemStack2)
                            }
                        }
                    pokemon.playSound(SoundEvents.BLOCK_STONE_BREAK, 1.0f, 1.0f)
                }
                if (collectable) {
                    pokemon.carriedBlocks.add(blockState!!.block.defaultState)
                    pokemon.playSound(SoundEvents.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f)
                }
            }
        }
    }
    override fun tick() {
        ++cooldown
        if (cooldown%intervalTick == 0) {
            if(!pokemon.isBattling && pokemon.blockY < 40)
                dig()

            cooldown = 0
        }

    }
}

internal class PlaceBlockPokemonGoal(private val pokemon: PokemonEntity) : Goal() {
    override fun canStart(): Boolean {
        return if (pokemon.carriedBlocks == null) {
            false
        } else if (!pokemon.world.gameRules.getBoolean(GameRules.DO_MOB_GRIEFING)) {
            false
        } else {
            pokemon.random.nextInt(toGoalTicks(2)) == 0
        }
    }

    override fun tick() {
        val random = pokemon.random
        val world = pokemon.world
        val i = MathHelper.floor(pokemon.x - 1.0 + random.nextDouble() * 2.0)
        val j = MathHelper.floor(pokemon.y + random.nextDouble() * 3.0)
        val k = MathHelper.floor(pokemon.z - 1.0 + random.nextDouble() * 2.0)
        val blockPos = BlockPos(i, j, k)
        val blockState = world.getBlockState(blockPos)
        val blockPos2 = blockPos.down()
        val blockState2 = world.getBlockState(blockPos2)
        var blockState3 = pokemon.carriedBlocks
        if (!blockState3.isEmpty()) {
            for (block in blockState3){
                val block = Block.postProcessState(block, pokemon.world, blockPos)
                if (canPlaceOn(world, blockPos, block, blockState, blockState2, blockPos2)) {
                    world.setBlockState(blockPos, block, 3)
                    world.emitGameEvent(
                        GameEvent.BLOCK_PLACE, blockPos, GameEvent.Emitter.of(
                            pokemon, block
                        )
                    )
                }
            }
            pokemon.carriedBlocks.clear()

        }
    }

    private fun canPlaceOn(
        world: World,
        posAbove: BlockPos,
        carriedState: BlockState?,
        stateAbove: BlockState,
        state: BlockState,
        pos: BlockPos
    ): Boolean {
        return stateAbove.isAir && !state.isAir && !state.isOf(Blocks.BEDROCK) && state.isFullCube(
            world,
            pos
        ) && carriedState!!.canPlaceAt(world, posAbove) && world.getOtherEntities(
            pokemon, Box.from(Vec3d.of(posAbove))
        ).isEmpty()
    }
}