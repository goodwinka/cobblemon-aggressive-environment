/*
 * Copyright (C) 2023 Cobblemon Contributors
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

package com.cobblemon.mod.common.entity.pokemon

import com.cobblemon.mod.common.*
import com.cobblemon.mod.common.CobblemonNetwork.sendPacket
import com.cobblemon.mod.common.api.drop.DropTable
import com.cobblemon.mod.common.api.entity.Despawner
import com.cobblemon.mod.common.api.events.CobblemonEvents
import com.cobblemon.mod.common.api.events.entity.PokemonEntityLoadEvent
import com.cobblemon.mod.common.api.events.entity.PokemonEntitySaveEvent
import com.cobblemon.mod.common.api.events.entity.PokemonEntitySaveToWorldEvent
import com.cobblemon.mod.common.api.events.pokemon.ShoulderMountEvent
import com.cobblemon.mod.common.api.interaction.PokemonEntityInteraction
import com.cobblemon.mod.common.api.net.serializers.PoseTypeDataSerializer
import com.cobblemon.mod.common.api.net.serializers.StringSetDataSerializer
import com.cobblemon.mod.common.api.pokemon.feature.FlagSpeciesFeature
import com.cobblemon.mod.common.api.pokemon.status.Statuses
import com.cobblemon.mod.common.api.reactive.ObservableSubscription
import com.cobblemon.mod.common.api.reactive.SimpleObservable
import com.cobblemon.mod.common.api.scheduling.afterOnMain
import com.cobblemon.mod.common.api.storage.InvalidSpeciesException
import com.cobblemon.mod.common.api.types.ElementalType
import com.cobblemon.mod.common.api.types.ElementalTypes
import com.cobblemon.mod.common.api.types.ElementalTypes.FIRE
import com.cobblemon.mod.common.battles.BagItems
import com.cobblemon.mod.common.battles.BattleRegistry
import com.cobblemon.mod.common.block.entity.PokemonPastureBlockEntity
import com.cobblemon.mod.common.client.entity.PokemonClientDelegate
import com.cobblemon.mod.common.entity.EntityProperty
import com.cobblemon.mod.common.entity.PoseType
import com.cobblemon.mod.common.entity.Poseable
import com.cobblemon.mod.common.entity.pokeball.EmptyPokeBallEntity
import com.cobblemon.mod.common.entity.pokemon.ai.PokemonMoveControl
import com.cobblemon.mod.common.entity.pokemon.ai.PokemonNavigation
import com.cobblemon.mod.common.entity.pokemon.ai.goals.*
import com.cobblemon.mod.common.net.messages.client.sound.PokemonCryPacket
import com.cobblemon.mod.common.net.messages.client.sound.UnvalidatedPlaySoundS2CPacket
import com.cobblemon.mod.common.net.messages.client.spawn.SpawnPokemonPacket
import com.cobblemon.mod.common.net.messages.client.ui.InteractPokemonUIPacket
import com.cobblemon.mod.common.net.serverhandling.storage.SendOutPokemonHandler.SEND_OUT_DURATION
import com.cobblemon.mod.common.pokemon.FormData
import com.cobblemon.mod.common.pokemon.Pokemon
import com.cobblemon.mod.common.pokemon.Species
import com.cobblemon.mod.common.pokemon.activestate.ActivePokemonState
import com.cobblemon.mod.common.pokemon.activestate.InactivePokemonState
import com.cobblemon.mod.common.pokemon.activestate.ShoulderedState
import com.cobblemon.mod.common.pokemon.ai.FormPokemonBehaviour
import com.cobblemon.mod.common.pokemon.evolution.variants.ItemInteractionEvolution
import com.cobblemon.mod.common.util.*
import com.cobblemon.mod.common.world.gamerules.CobblemonGameRules
import net.minecraft.entity.*
import net.minecraft.entity.ai.control.MoveControl
import net.minecraft.entity.ai.goal.*
import net.minecraft.entity.ai.pathing.EntityNavigation
import net.minecraft.entity.ai.pathing.PathNodeType
import net.minecraft.entity.attribute.DefaultAttributeContainer
import net.minecraft.entity.attribute.EntityAttributes
import net.minecraft.entity.damage.DamageSource
import net.minecraft.entity.damage.DamageTypes
import net.minecraft.entity.data.DataTracker
import net.minecraft.entity.data.TrackedData
import net.minecraft.entity.data.TrackedDataHandlerRegistry
import net.minecraft.entity.effect.StatusEffectInstance
import net.minecraft.entity.effect.StatusEffects
import net.minecraft.entity.mob.Angerable
import net.minecraft.entity.passive.AnimalEntity
import net.minecraft.entity.passive.PassiveEntity
import net.minecraft.entity.passive.TameableShoulderEntity
import net.minecraft.entity.player.PlayerEntity
import net.minecraft.entity.projectile.*
import net.minecraft.fluid.FluidState
import net.minecraft.item.*
import net.minecraft.nbt.NbtCompound
import net.minecraft.nbt.NbtHelper
import net.minecraft.nbt.NbtString
import net.minecraft.network.listener.ClientPlayPacketListener
import net.minecraft.network.packet.Packet
import net.minecraft.network.packet.s2c.play.EntitySpawnS2CPacket
import net.minecraft.particle.DefaultParticleType
import net.minecraft.particle.ParticleTypes
import net.minecraft.registry.RegistryKeys
import net.minecraft.registry.tag.FluidTags
import net.minecraft.server.network.ServerPlayerEntity
import net.minecraft.server.world.ChunkTicketType
import net.minecraft.server.world.ServerWorld
import net.minecraft.sound.SoundCategory
import net.minecraft.sound.SoundEvents
import net.minecraft.text.Text
import net.minecraft.text.TextContent
import net.minecraft.util.*
import net.minecraft.util.math.*
import net.minecraft.util.math.intprovider.UniformIntProvider
import net.minecraft.world.EntityView
import net.minecraft.world.World
import net.minecraft.world.event.GameEvent

import java.util.EnumSet
import java.util.Optional
import java.util.*
import java.util.concurrent.CompletableFuture
import java.util.function.Predicate

import net.minecraft.block.BlockState
import net.minecraft.enchantment.Enchantments
import net.minecraft.loot.context.LootContextParameterSet
import net.minecraft.loot.context.LootContextParameters


@Suppress("unused")
class PokemonEntity(
    world: World,
    pokemon: Pokemon = Pokemon(),
    type: EntityType<out PokemonEntity> = CobblemonEntities.POKEMON,
) : TameableShoulderEntity(type, world), Poseable, Shearable, Angerable {
//) : TameableShoulderEntity(type, world),ZombieEntity(type, world), Poseable, Shearable {
    val removalObservable = SimpleObservable<RemovalReason?>()
    /** A list of observable subscriptions related to this entity that need to be cleaned up when the entity is removed. */
    val subscriptions = mutableListOf<ObservableSubscription<*>>()
    private var ANGER_TIME: Int =  0
    private var angryAt: UUID? = null
    private var BLAZE_FLAGS: Byte = 0.toByte()

    private var ANGER_TIME_RANGE: UniformIntProvider = TimeHelper.betweenSeconds(20, 39)

    private var SHOOTING: Boolean = false
    private var IS_AGGESSIVE: Boolean = false
    private var fireballStrength = 1
    var WARMUP_TIME: Int = 80
    val carriedBlocks :  MutableList<BlockState> = mutableListOf<BlockState>()
    var carriedBlock :  BlockState? = null

    fun isShooting(): Boolean {
        return SHOOTING
    }

    fun setShooting(shooting: Boolean) {
        SHOOTING= shooting
    }

    fun getFireballStrength(): Int {
        return this.fireballStrength
    }

    val form: FormData
        get() = pokemon.form
    val behaviour: FormPokemonBehaviour
        get() = form.behaviour

    var pokemon: Pokemon = pokemon
        set(value) {
            field = value
            delegate.changePokemon(value)
            // We need to update this value every time the Pokémon changes, other eye height related things will be dynamic.
            this.updateEyeHeight()
        }

    var despawner: Despawner<PokemonEntity> = Cobblemon.bestSpawner.defaultPokemonDespawner

    /** The player that caused this Pokémon to faint. */
    var killer: ServerPlayerEntity? = null

    var ticksLived = 0
    val busyLocks = mutableListOf<Any>()
    val isBusy: Boolean
        get() = busyLocks.isNotEmpty()
    val isBattling: Boolean
        get() = this.battleId.get().isPresent

    var drops: DropTable? = null

    var tethering: PokemonPastureBlockEntity.Tethering? = null

    /**
     * The amount of steps this entity has traveled.
     */
    var blocksTraveled: Double = 0.0

    val entityProperties = mutableListOf<EntityProperty<*>>()

    val species = addEntityProperty(SPECIES, pokemon.species.resourceIdentifier.toString())
    val nickname = addEntityProperty(NICKNAME, pokemon.nickname ?: Text.empty())
    val nicknameVisible = addEntityProperty(NICKNAME_VISIBLE, true)
    val shouldRenderName = addEntityProperty(SHOULD_RENDER_NAME, true)
    val isMoving = addEntityProperty(MOVING, false)
    val behaviourFlags = addEntityProperty(BEHAVIOUR_FLAGS, 0)
    val phasingTargetId = addEntityProperty(PHASING_TARGET_ID, -1)
    val battleId = addEntityProperty(BATTLE_ID, Optional.empty())
    val aspects = addEntityProperty(ASPECTS, pokemon.aspects)
    val deathEffectsStarted = addEntityProperty(DYING_EFFECTS_STARTED, false)
    val poseType = addEntityProperty(POSE_TYPE, PoseType.STAND)
    internal val labelLevel = addEntityProperty(LABEL_LEVEL, pokemon.level)
    val hideLabel = addEntityProperty(HIDE_LABEL, false)
    val unbattleable = addEntityProperty(UNBATTLEABLE, false)

    /**
     * 0 is do nothing,
     * 1 is appearing from a pokeball so needs to be small then grows,
     * 2 is being captured/recalling so starts large and shrinks.
     */
    val beamModeEmitter = addEntityProperty(BEAM_MODE, 0.toByte())
    // properties like the above are synced and can be subscribed to for changes on either side

    val delegate = if (world.isClient) {
        // Don't import because scanning for imports is a CI job we'll do later to detect errant access to client from server
        PokemonClientDelegate()
    } else {
        PokemonServerDelegate()
    }
    override fun initDataTracker() {
        //dataTracker.startTracking(ANGER_TIME, 0)
        super.initDataTracker()

    }
    init {


        delegate.initialize(this)
        delegate.changePokemon(pokemon)
        calculateDimensions()

        subscriptions.add(
            battleId
                .subscribeIncludingCurrent {
                    if (it.isPresent) {
                        busyLocks.add(BATTLE_LOCK)
                    } else {
                        busyLocks.remove(BATTLE_LOCK)
                    }
                }
        )

        subscriptions.add(
            poseType.subscribe {
                if (it == PoseType.FLY || it == PoseType.HOVER) {
                    setNoGravity(true)
                } else {
                    setNoGravity(false)
                }
            }
        )

        subscriptions.add(
            species.subscribe {
                calculateDimensions()
            }
        )
    }

    companion object {
        val SPECIES = DataTracker.registerData(PokemonEntity::class.java, TrackedDataHandlerRegistry.STRING)
        val NICKNAME = DataTracker.registerData(PokemonEntity::class.java, TrackedDataHandlerRegistry.TEXT_COMPONENT)
        val NICKNAME_VISIBLE = DataTracker.registerData(PokemonEntity::class.java, TrackedDataHandlerRegistry.BOOLEAN)
        val SHOULD_RENDER_NAME = DataTracker.registerData(PokemonEntity::class.java, TrackedDataHandlerRegistry.BOOLEAN)
        val MOVING = DataTracker.registerData(PokemonEntity::class.java, TrackedDataHandlerRegistry.BOOLEAN)
        val BEHAVIOUR_FLAGS = DataTracker.registerData(PokemonEntity::class.java, TrackedDataHandlerRegistry.BYTE)
        val PHASING_TARGET_ID = DataTracker.registerData(PokemonEntity::class.java, TrackedDataHandlerRegistry.INTEGER)
        val BEAM_MODE = DataTracker.registerData(PokemonEntity::class.java, TrackedDataHandlerRegistry.BYTE)
        val BATTLE_ID = DataTracker.registerData(PokemonEntity::class.java, TrackedDataHandlerRegistry.OPTIONAL_UUID)
        val ASPECTS = DataTracker.registerData(PokemonEntity::class.java, StringSetDataSerializer)
        val DYING_EFFECTS_STARTED = DataTracker.registerData(PokemonEntity::class.java, TrackedDataHandlerRegistry.BOOLEAN)
        val POSE_TYPE = DataTracker.registerData(PokemonEntity::class.java, PoseTypeDataSerializer)
        val LABEL_LEVEL = DataTracker.registerData(PokemonEntity::class.java, TrackedDataHandlerRegistry.INTEGER)
        val HIDE_LABEL = DataTracker.registerData(PokemonEntity::class.java, TrackedDataHandlerRegistry.BOOLEAN)
        val UNBATTLEABLE = DataTracker.registerData(PokemonEntity::class.java, TrackedDataHandlerRegistry.BOOLEAN)

        const val BATTLE_LOCK = "battle"

        fun createAttributes(): DefaultAttributeContainer.Builder = LivingEntity.createLivingAttributes()
            .add(EntityAttributes.GENERIC_FOLLOW_RANGE).add(EntityAttributes.GENERIC_ATTACK_DAMAGE, 2.0)
            .add(EntityAttributes.GENERIC_ATTACK_KNOCKBACK)

    }

    override fun canWalkOnFluid(state: FluidState): Boolean {
//        val node = navigation.currentPath?.currentNode
//        val targetPos = node?.blockPos
//        if (targetPos == null || world.getBlockState(targetPos.up()).isAir) {
        return if (state.isIn(FluidTags.WATER)) {
            behaviour.moving.swim.canWalkOnWater
        } else if (state.isIn(FluidTags.LAVA)) {
            behaviour.moving.swim.canWalkOnLava
        } else {
            super.canWalkOnFluid(state)
        }
//        }
//
//        return super.canWalkOnFluid(state)
    }

    override fun handleStatus(status: Byte) {
        delegate.handleStatus(status)
        super.handleStatus(status)
    }
    override fun getAngerTime(): Int {
        return ANGER_TIME
        //return dataTracker.get(ANGER_TIME) as Int
    }

    override fun setAngerTime(angerTime: Int) {
        ANGER_TIME= angerTime
        //dataTracker.set(ANGER_TIME, angerTime)
    }
    override fun getAngryAt(): UUID? {
        return angryAt
    }
    override fun setAngryAt(angryAt: UUID?) {
        if(!isBattling && (pokemon.getOwnerPlayer() == null || world.getPlayerByUuid(angryAt) == null))
            this.angryAt = angryAt
    }
    override fun chooseRandomAngerTime() {
        this.angerTime = ANGER_TIME_RANGE[random]
    }
    fun applyStatusEffect(type: ElementalType?,target:LivingEntity){
        when(type){
            ElementalTypes.NORMAL->{}
            ElementalTypes.FIGHTING->{}
            ElementalTypes.FLYING->{}
            ElementalTypes.POISON->{target.addStatusEffect(StatusEffectInstance(StatusEffects.POISON, 300, 0), this)}
            ElementalTypes.GROUND->{}
            ElementalTypes.ROCK->{}
            ElementalTypes.BUG->{target.addStatusEffect(StatusEffectInstance(StatusEffects.HUNGER, 300, 0), this)}
            ElementalTypes.GHOST->{}
            ElementalTypes.STEEL->{}
            ElementalTypes.FIRE->{}
            ElementalTypes.WATER->{}
            ElementalTypes.GRASS->{}
            ElementalTypes.ELECTRIC->{}
            ElementalTypes.PSYCHIC->{}
            ElementalTypes.ICE->{target.addStatusEffect(StatusEffectInstance(StatusEffects.SLOWNESS, 300, 0), this)
                target.addStatusEffect(StatusEffectInstance(StatusEffects.MINING_FATIGUE, 300, 0), this)
            }
            ElementalTypes.DRAGON->{}
            ElementalTypes.DARK->{target.addStatusEffect(StatusEffectInstance(StatusEffects.DARKNESS, 300, 0), this)
                target.addStatusEffect(StatusEffectInstance(StatusEffects.BLINDNESS, 300, 0), this)}
            ElementalTypes.FAIRY->{target.addStatusEffect(StatusEffectInstance(StatusEffects.WEAKNESS, 300, 0), this)}
            else ->{}
        }
        //LOGGER.info("applyStatusEffect ${type.toString()}")
    }
    override fun tryAttack(target: Entity): Boolean {
        val bl = target.damage(
            this.damageSources.mobAttack(this), this.getAttributeValue(EntityAttributes.GENERIC_ATTACK_DAMAGE).toInt()
                .toFloat()
        )
        //LOGGER.info("tryAttack ${target.entityName} ${pokemon.primaryType} ${pokemon.secondaryType}")
        if (bl) {
            applyDamageEffects(this, target)
            if(target as? LivingEntity != null){
                applyStatusEffect(pokemon.primaryType, target)
                applyStatusEffect(pokemon.secondaryType, target)
            }

        }
        return bl
    }

    fun teleportRandomly(): Boolean {
        return if (!world.isClient() && this.isAlive) {
            val d = this.x + (random.nextDouble() - 0.5) * 16.0
            val e = this.y + (random.nextInt(64) - 32).toDouble()
            val f = this.z + (random.nextDouble() - 0.5) * 16.0
            this.teleportTo(d, e, f)
        } else {
            false
        }
    }
    fun teleportTo(entity: Entity): Boolean {
        var vec3d = Vec3d(this.x - entity.x, getBodyY(0.5) - entity.eyeY, this.z - entity.z)
        vec3d = vec3d.normalize()
        val e = this.x + (random.nextDouble() - 0.5) * 8.0 - vec3d.x * 16.0
        val f = this.y + (random.nextInt(16) - 8).toDouble() - vec3d.y * 16.0
        val g = this.z + (random.nextDouble() - 0.5) * 8.0 - vec3d.z * 16.0
        return this.teleportTo(e, f, g)
    }

    private fun teleportTo(x: Double, y: Double, z: Double): Boolean {
        val mutable = BlockPos.Mutable(x, y, z)
        while (mutable.y > world.bottomY && !world.getBlockState(mutable).blocksMovement()) {
            mutable.move(Direction.DOWN)
        }
        val blockState = world.getBlockState(mutable)
        val bl = blockState.blocksMovement()
        val bl2 = blockState.fluidState.isIn(FluidTags.WATER)
        return if (bl && !bl2) {
            val vec3d = pos
            val bl3 = this.teleport(x, y, z, true)
            if (bl3) {
                world.emitGameEvent(GameEvent.TELEPORT, vec3d, GameEvent.Emitter.of(this))
                if (!this.isSilent) {
                    world.playSound(
                        null as PlayerEntity?,
                        prevX,
                        prevY,
                        prevZ, SoundEvents.ENTITY_ENDERMAN_TELEPORT, this.soundCategory, 1.0f, 1.0f
                    )
                    playSound(SoundEvents.ENTITY_ENDERMAN_TELEPORT, 1.0f, 1.0f)
                }
            }
            bl3
        } else {
            false
        }
    }

    override fun setTarget(target: LivingEntity?) {
        if(pokemon.getOwnerPlayer() == null){
            if(!isBattling){
                if(target as? ServerPlayerEntity != null)
                    for(poke in Cobblemon.storage.getParty(target).iterator()){
                        if(poke.entity != null)
                            if((poke.entity as PokemonEntity).isBattling)
                                return
                    }
                super.setTarget(target)
            }
        }
        else{
            if(target as? PokemonEntity != null)
                if(target.owner != null)
                    return
            if(!isBattling && target as? PlayerEntity == null)
                super.setTarget(target)
        }
    }

    override fun tick() {
        super.tick()
        // We will be handling idle logic ourselves thank you
        this.setDespawnCounter(0)
        entityProperties.forEach { it.checkForUpdate() }
        delegate.tick(this)
        ticksLived++
        if (this.ticksLived % 20 == 0) {
            this.updateEyeHeight()
            if(isBattling) {
                //LOGGER.info("in battle")
                stopAnger()

                for(selector in targetSelector.goals){
                    selector.stop()
                }
                super.setTarget(null)
            }
            if(target != null){
                if(target as? ServerPlayerEntity != null)
                    for(poke in Cobblemon.storage.getParty(target as ServerPlayerEntity).iterator()){
                        if(poke.entity != null)
                            if((poke.entity as PokemonEntity).isBattling)
                                super.setTarget(null)
                    }
            }
            getAttributeInstance(EntityAttributes.GENERIC_ATTACK_DAMAGE)!!.baseValue =pokemon.attack.toDouble()  / 15.0
            if(pokemon.level > (20).toInt() && pokemon.getOwnerPlayer() == null) {
                if (pokemon.attack > pokemon.specialAttack) {
                    IS_AGGESSIVE = pokemon.attack.toFloat() / pokemon.defence.toFloat() > 1.34f
                } else {
                    IS_AGGESSIVE = pokemon.specialAttack.toFloat() / pokemon.specialDefence.toFloat() > 1.34f
                }
            }else
                IS_AGGESSIVE = false
            //getAttributeInstance(EntityAttributes.GENERIC_MOVEMENT_SPEED)!!.baseValue =pokemon.speed.toDouble()
            if(world as? ServerWorld != null && pokemon.getOwnerPlayer() == null){
                var skillParticle : DefaultParticleType = ParticleTypes.HEART
                if(IS_AGGESSIVE || this.target as? PlayerEntity != null)
                    skillParticle = ParticleTypes.SOUL_FIRE_FLAME
                val vec3d: Vec3d = getEyePos()
                (world as ServerWorld).spawnParticles<DefaultParticleType>(
                    skillParticle,
                    vec3d.x+0.0,
                    vec3d.y+1.0,
                    vec3d.z+0.0,
                    1,
                    0.0,
                    0.0,
                    0.0,
                    0.0
                )
            }
        }

        if (this.tethering != null && !this.tethering!!.box.contains(this.x, this.y, this.z)) {
            this.tethering = null
            this.pokemon.recall()
        }
    }



    fun setMoveControl(moveControl: MoveControl) {
        this.moveControl = moveControl
    }

    /**
     * Prevents water type Pokémon from taking drowning damage.
     */
    override fun canBreatheInWater(): Boolean {
        return behaviour.moving.swim.canBreatheUnderwater
    }

    /**
     * Prevents fire type Pokémon from taking fire damage.
     */
    override fun isFireImmune(): Boolean {
        return FIRE in pokemon.types || !behaviour.moving.swim.hurtByLava
    }

    /**
     * Prevents flying type Pokémon from taking fall damage.
     */
    override fun handleFallDamage(fallDistance: Float, damageMultiplier: Float, damageSource: DamageSource?): Boolean {
        return if (ElementalTypes.FLYING in pokemon.types || pokemon.ability.name == "levitate" || pokemon.species.behaviour.moving.fly.canFly) {
            false
        } else {
            super.handleFallDamage(fallDistance, damageMultiplier, damageSource)
        }
    }

    override fun isInvulnerableTo(damageSource: DamageSource): Boolean {
        // If the entity is busy, it cannot be hurt.
        if (busyLocks.isNotEmpty()) {
            return true
        }

        // Owned Pokémon cannot be hurt by players or suffocation
        if (ownerUuid != null && (damageSource.attacker is PlayerEntity || damageSource.isOf(DamageTypes.IN_WALL))) {
            return true
        }

        if (!Cobblemon.config.playerDamagePokemon && damageSource.attacker is PlayerEntity) {
            return true
        }

        return super.isInvulnerableTo(damageSource)
    }

    /**
     * A utility method that checks if this Pokémon has the [UncatchableProperty.uncatchable] property.
     *
     * @return If the Pokémon is uncatchable.
     */
    fun isUncatchable() = pokemon.isUncatchable()

    fun dropAllCollectables(){
        if (!carriedBlocks.isEmpty() && world is ServerWorld) {
            for (block in carriedBlocks) {
                val itemStack = ItemStack(Items.DIAMOND_AXE)
                itemStack.addEnchantment(Enchantments.SILK_TOUCH, 1)
                val builder = LootContextParameterSet.Builder(world as ServerWorld?)
                    .add<Vec3d>(LootContextParameters.ORIGIN, pos)
                    .add<ItemStack>(LootContextParameters.TOOL, itemStack).addOptional<Entity>(
                        LootContextParameters.THIS_ENTITY, this
                    )
                val list = block.getDroppedStacks(builder)
                val var8: Iterator<*> = list.iterator()
                while (var8.hasNext()) {
                    val itemStack2 = var8.next() as ItemStack
                    dropStack(itemStack2)
                }
            }
            carriedBlocks.clear()
        }
    }

    fun recallWithAnimation(): CompletableFuture<Pokemon> {
        val owner = owner
        val future = CompletableFuture<Pokemon>()
        if (phasingTargetId.get() == -1 && owner != null) {
            dropAllCollectables()
            owner.getWorld().playSoundServer(pos, CobblemonSounds.POKE_BALL_RECALL, volume = 0.6F)
            phasingTargetId.set(owner.id)
            beamModeEmitter.set(2)
            val state = pokemon.state;
            afterOnMain(seconds = SEND_OUT_DURATION) {
                // only recall if the pokemon hasn't been recalled yet for this state
                if (state == pokemon.state) {
                    pokemon.recall()
                }
                future.complete(pokemon)
            }
        } else {
            pokemon.recall()
            future.complete(pokemon)
        }

        return future
    }

    override fun writeNbt(nbt: NbtCompound): NbtCompound {
        val tethering = this.tethering
        if (tethering != null) {
            val tetheringNbt = NbtCompound()
            tetheringNbt.putUuid(DataKeys.TETHERING_ID, tethering.tetheringId)
            tetheringNbt.putUuid(DataKeys.POKEMON_UUID, tethering.pokemonId)
            tetheringNbt.putUuid(DataKeys.POKEMON_OWNER_ID, tethering.playerId)
            tetheringNbt.putUuid(DataKeys.PC_ID, tethering.pcId)
            tetheringNbt.put(DataKeys.TETHER_MIN_ROAM_POS, NbtHelper.fromBlockPos(tethering.minRoamPos))
            tetheringNbt.put(DataKeys.TETHER_MAX_ROAM_POS, NbtHelper.fromBlockPos(tethering.maxRoamPos))
            nbt.put(DataKeys.TETHERING, tetheringNbt)
        } else {
            nbt.put(DataKeys.POKEMON, pokemon.saveToNBT(NbtCompound()))
        }
        val battleIdToSave = battleId.get().orElse(null)
        if (battleIdToSave != null) {
            nbt.putUuid(DataKeys.POKEMON_BATTLE_ID, battleIdToSave)
        }
        nbt.putString(DataKeys.POKEMON_POSE_TYPE, poseType.get().name)
        nbt.putByte(DataKeys.POKEMON_BEHAVIOUR_FLAGS, behaviourFlags.get())

        if (hideLabel.get()) {
            nbt.putBoolean(DataKeys.POKEMON_HIDE_LABEL, true)
        }
        if (unbattleable.get()) {
            nbt.putBoolean(DataKeys.POKEMON_UNBATTLEABLE, true)
        }

        CobblemonEvents.POKEMON_ENTITY_SAVE.post(PokemonEntitySaveEvent(this, nbt))

        return super.writeNbt(nbt)
    }

    override fun readNbt(nbt: NbtCompound) {
        super.readNbt(nbt)
        if (nbt.contains(DataKeys.TETHERING)) {
            val tetheringNBT = nbt.getCompound(DataKeys.TETHERING)
            val tetheringId = tetheringNBT.getUuid(DataKeys.TETHERING_ID)
            val pcId = tetheringNBT.getUuid(DataKeys.PC_ID)
            val pokemonId = tetheringNBT.getUuid(DataKeys.POKEMON_UUID)
            val playerId = tetheringNBT.getUuid(DataKeys.POKEMON_OWNER_ID)
            val minRoamPos = NbtHelper.toBlockPos(tetheringNBT.getCompound(DataKeys.TETHER_MIN_ROAM_POS))
            val maxRoamPos = NbtHelper.toBlockPos(tetheringNBT.getCompound(DataKeys.TETHER_MAX_ROAM_POS))

            val loadedPokemon = Cobblemon.storage.getPC(pcId)[pokemonId]
            if (loadedPokemon != null && loadedPokemon.tetheringId == tetheringId) {
                pokemon = loadedPokemon
                tethering = PokemonPastureBlockEntity.Tethering(
                    minRoamPos = minRoamPos,
                    maxRoamPos = maxRoamPos,
                    playerId = playerId,
                    playerName = "",
                    tetheringId = tetheringId,
                    pokemonId = pokemonId,
                    pcId = pcId,
                    entityId = id // Doesn't really matter on the entity
                )
            } else {
                pokemon = Pokemon()
                health = 0F
            }
        } else {
            pokemon = try {
                Pokemon().loadFromNBT(nbt.getCompound(DataKeys.POKEMON))
            } catch (_: InvalidSpeciesException) {
                health = 0F
                Pokemon()
            }
        }
        species.set(pokemon.species.resourceIdentifier.toString())
        nickname.set(pokemon.nickname ?: Text.empty())
        labelLevel.set(pokemon.level)
        val savedBattleId = if (nbt.containsUuid(DataKeys.POKEMON_BATTLE_ID)) nbt.getUuid(DataKeys.POKEMON_BATTLE_ID) else null
        if (savedBattleId != null) {
            val battle = BattleRegistry.getBattle(savedBattleId)
            if (battle != null) {
                battleId.set(Optional.of(savedBattleId))
            }
        }
        poseType.set(PoseType.valueOf(nbt.getString(DataKeys.POKEMON_POSE_TYPE)))
        behaviourFlags.set(nbt.getByte(DataKeys.POKEMON_BEHAVIOUR_FLAGS))
        this.setBehaviourFlag(PokemonBehaviourFlag.EXCITED, true)

        if (nbt.contains(DataKeys.POKEMON_HIDE_LABEL)) {
            hideLabel.set(nbt.getBoolean(DataKeys.POKEMON_HIDE_LABEL))
        }
        if (nbt.contains(DataKeys.POKEMON_UNBATTLEABLE)) {
            unbattleable.set(nbt.getBoolean(DataKeys.POKEMON_UNBATTLEABLE))
        }

        CobblemonEvents.POKEMON_ENTITY_LOAD.postThen(
            event = PokemonEntityLoadEvent(this, nbt),
            ifSucceeded = {},
            ifCanceled = { this.discard() }
        )
    }

    override fun createSpawnPacket(): Packet<ClientPlayPacketListener> = CobblemonNetwork.asVanillaClientBound(SpawnPokemonPacket(this, super.createSpawnPacket() as EntitySpawnS2CPacket))

    override fun getPathfindingPenalty(nodeType: PathNodeType): Float {
        return if (nodeType == PathNodeType.OPEN) 2F else super.getPathfindingPenalty(nodeType)
    }

    override fun getNavigation(): PokemonNavigation {
        return navigation as PokemonNavigation
    }

    override fun createNavigation(world: World): EntityNavigation {
        return PokemonNavigation(world, this)
    }

    fun checkAgressive(entity: LivingEntity): Boolean{
        return IS_AGGESSIVE && !isBattling && distanceTo(entity)<10.0f && canSee(entity)
        //if(!IS_AGGESSIVE)
        //    return shouldAngerAt(entity)
        //return true
    }

    @Suppress("SENSELESS_COMPARISON")
    public override fun initGoals() {
        // DO NOT REMOVE
        // LivingEntity#getActiveEyeHeight is called in the constructor of Entity
        // Pokémon param is not available yet
        if (this.pokemon == null) {
            return
        }
        moveControl = PokemonMoveControl(this)

        setTamed( pokemon.getOwnerPlayer() != null)

        //getAttributeInstance(EntityAttributes.GENERIC_MOVEMENT_SPEED)!!.baseValue =pokemon.speed.toDouble()/360.0+0.6


        goalSelector.clear { true }
        goalSelector.add(0, PokemonInBattleMovementGoal(this, 10))
        goalSelector.add(0, object : Goal() {
            override fun canStart() = this@PokemonEntity.phasingTargetId.get() != -1 || pokemon.status?.status == Statuses.SLEEP || deathEffectsStarted.get()
            override fun shouldContinue(): Boolean {
                if (pokemon.status?.status == Statuses.SLEEP && !canSleep() && !isBusy) {
                    return false
                } else if (pokemon.status?.status == Statuses.SLEEP || isBusy) {
                    return true
                }
                return false
            }
            override fun getControls() = EnumSet.allOf(Control::class.java)
        })

        goalSelector.add(1, PokemonBreatheAirGoal(this))
        goalSelector.add(2, PokemonFloatToSurfaceGoal(this))
        goalSelector.add(5, PokemonFollowOwnerGoal(this, 1.0, 8F, 2F, false))

        if((pokemon.primaryType == ElementalTypes.ROCK || pokemon.secondaryType == ElementalTypes.ROCK) && owner != null) {
            goalSelector.add(3, PickUpBlockPokemonGoal(this))
            //goalSelector.add(3, PlaceBlockPokemonGoal(this))
        }
        goalSelector.add(4, PounceAtTargetGoal(this, 0.4f))
        goalSelector.add(4, MeleeAttackGoal(this, getAttributeInstance(EntityAttributes.GENERIC_MOVEMENT_SPEED)!!.baseValue, false))
        goalSelector.add(4, PokemonSelfCastGoal(this))
        if(pokemon.primaryType == ElementalTypes.PSYCHIC || pokemon.secondaryType == ElementalTypes.PSYCHIC)
            goalSelector.add(4, PokemonTeleportGoal(this))
        //if(pokemon.specialAttack>pokemon.attack)
        if(true){
            if(pokemon.primaryType == ElementalTypes.FIRE || pokemon.secondaryType == ElementalTypes.FIRE ||
                pokemon.primaryType == ElementalTypes.DRAGON || pokemon.secondaryType == ElementalTypes.DRAGON){
                goalSelector.add(4, ShootFireballGoalGhast(this))
            }
            if(pokemon.primaryType == ElementalTypes.FIRE || pokemon.secondaryType == ElementalTypes.FIRE ||
                pokemon.primaryType == ElementalTypes.DRAGON || pokemon.secondaryType == ElementalTypes.DRAGON){
                goalSelector.add(4, ShootFireballGoal(this))
            }
            goalSelector.add(4, CustomFireBeamGoal(this))
        }

        goalSelector.add(
            9, LookAtEntityGoal(
                this,
                PlayerEntity::class.java, 8.0f
            )
        )

        goalSelector.add(10, PokemonMoveIntoFluidGoal(this))
        goalSelector.add(11, SleepOnTrainerGoal(this))
        goalSelector.add(12, WildRestGoal(this))

        if (pokemon.getFeature<FlagSpeciesFeature>(DataKeys.HAS_BEEN_SHEARED) != null) {
            goalSelector.add(13, EatGrassGoal(this))
        }

        goalSelector.add(14, PokemonWanderAroundGoal(this))
        goalSelector.add(15, PokemonLookAtEntityGoal(this, ServerPlayerEntity::class.java, 5F))
        goalSelector.add(16, PokemonPointAtSpawnGoal(this))

        targetSelector.add(1, TrackOwnerAttackerGoal(this))
        targetSelector.add(3, RevengeGoal(this, *arrayOfNulls(0)).setGroupRevenge(*arrayOfNulls(0)))
        targetSelector.add(2, AttackWithOwnerGoal(this))
        targetSelector.add(4, ActiveTargetGoal<PlayerEntity>(this,
            PlayerEntity::class.java, 100, true, false,
            Predicate<LivingEntity> { entity: LivingEntity -> checkAgressive(entity)})
        )
    }

    fun canSleep(): Boolean {
        val rest = behaviour.resting
        val worldTime = (world.timeOfDay % 24000).toInt()
        val light = world.getLightLevel(blockPos)
        val block = world.getBlockState(blockPos).block
        val biome = world.getBiome(blockPos).value()

        return rest.canSleep &&
                !this.getBehaviourFlag(PokemonBehaviourFlag.EXCITED) &&
                worldTime in this.behaviour.resting.times &&
                light in rest.light &&
                (rest.blocks.isEmpty() || rest.blocks.any { it.fits(block, this.world.registryManager.get(RegistryKeys.BLOCK)) }) &&
                (rest.biomes.isEmpty() || rest.biomes.any { it.fits(biome, this.world.registryManager.get(RegistryKeys.BIOME)) })
    }

    fun <T> addEntityProperty(accessor: TrackedData<T>, initialValue: T): EntityProperty<T> {
        val property = EntityProperty(
            dataTracker = dataTracker,
            accessor = accessor,
            initialValue = initialValue
        )
        entityProperties.add(property)
        return property
    }

    override fun createChild(level: ServerWorld, partner: PassiveEntity) = null

    override fun isReadyToSitOnPlayer(): Boolean {
        return pokemon.form.shoulderMountable
    }

    override fun interactMob(player: PlayerEntity, hand: Hand) : ActionResult {
        val itemStack = player.getStackInHand(hand)

        if (itemStack.isOf(Items.SHEARS) && this.isShearable) {
            this.sheared(SoundCategory.PLAYERS)
            this.emitGameEvent(GameEvent.SHEAR, player)
            itemStack.damage(1, player) { it.sendToolBreakStatus(hand) }
            return ActionResult.SUCCESS
        }
        else if (itemStack.isOf(Items.BUCKET)) {
            if (pokemon.getFeature<FlagSpeciesFeature>(DataKeys.CAN_BE_MILKED) != null) {
                player.playSound(SoundEvents.ENTITY_GOAT_MILK, 1.0f, 1.0f)
                val milkBucket = ItemUsage.exchangeStack(itemStack, player, Items.MILK_BUCKET.defaultStack)
                player.setStackInHand(hand, milkBucket)
                return ActionResult.success(world.isClient)
            }
        } else if (itemStack.isOf(Items.BOWL)) {
            if (pokemon.getFeature<FlagSpeciesFeature>(DataKeys.IS_MOOSHTANK) != null) {
                player.playSound(SoundEvents.ENTITY_MOOSHROOM_MILK, 1.0f, 1.0f)
                val mushroomStew = ItemUsage.exchangeStack(itemStack, player, Items.MUSHROOM_STEW.defaultStack)
                player.setStackInHand(hand, mushroomStew)
                return ActionResult.success(world.isClient)
            }
            else if (pokemon.species.name == "Shuckle") {
                player.playSound(SoundEvents.ENTITY_MOOSHROOM_MILK, 1.0f, 1.0f)
                val berryJuice = ItemUsage.exchangeStack(itemStack, player, CobblemonItems.BERRY_JUICE.defaultStack)
                player.setStackInHand(hand, berryJuice)
                return ActionResult.success(world.isClient)
            }
        }

        if (hand == Hand.MAIN_HAND && player is ServerPlayerEntity && pokemon.getOwnerPlayer() == player) {
            if (player.isSneaking) {
                InteractPokemonUIPacket(this.getUuid(), isReadyToSitOnPlayer && pokemon in player.party()).sendToPlayer(player)
            } else {
                // TODO #105
                if (this.attemptItemInteraction(player, player.getStackInHand(hand))) return ActionResult.SUCCESS
            }
        }

        return super.interactMob(player, hand)
    }

    override fun getDimensions(pose: EntityPose): EntityDimensions {
        val scale = pokemon.form.baseScale * pokemon.scaleModifier
        return pokemon.form.hitbox.scaled(scale)
    }

    override fun canTakeDamage() = super.canTakeDamage() && !isBusy
    override fun damage(source: DamageSource?, amount: Float): Boolean {
        val entity = source!!.attacker
        if(entity as? PokemonEntity == this || entity as? LivingEntity == owner)
            return false

        if (entity != null && entity is PlayerEntity && entity !is PersistentProjectileEntity) {
            setAngryAt(entity.uuid)
        }

//        LOGGER.info("${pokemon.getDisplayName()} hp:${pokemon.currentHealth}" +
//                "health:${health}" +
//                "atk:${pokemon.attack}" +
//                " def:${pokemon.defence}" +
//                " spatk:${pokemon.specialAttack}" +
//                " spdef:${pokemon.specialDefence}" +
//                " spd:${pokemon.speed}" +
//                " lvl:${pokemon.level}")
        if(amount <= 2.0f && (pokemon.primaryType == ElementalTypes.DRAGON || pokemon.secondaryType == ElementalTypes.DRAGON))
            return false
        var amount2 = amount / (pokemon.defence.toFloat()/50.0f)
        return if (super.damage(source, amount2)) {
            if (this.health == 0F) {
                pokemon.currentHealth = 0
            } else {
//                pokemon.currentHealth = this.health.toInt()
            }
            true
        } else false
    }

    override fun shouldSave(): Boolean {
        if (ownerUuid == null && (Cobblemon.config.savePokemonToWorld || isPersistent)) {
            CobblemonEvents.POKEMON_ENTITY_SAVE_TO_WORLD.postThen(PokemonEntitySaveToWorldEvent(this)) {
                return true
            }
        }
        return tethering != null
    }

    override fun checkDespawn() {
        if (pokemon.getOwnerUUID() == null && !isPersistent && despawner.shouldDespawn(this)) {
            discard()
        }
    }

    override fun getOwnerUuid(): UUID?{
        return pokemon.getOwnerUUID()
    }

    override fun getEyeHeight(pose: EntityPose): Float = this.pokemon.form.eyeHeight(this)

    @Suppress("SENSELESS_COMPARISON")
    override fun getActiveEyeHeight(pose: EntityPose, dimensions: EntityDimensions): Float {
        // DO NOT REMOVE
        // LivingEntity#getActiveEyeHeight is called in the constructor of Entity
        // Pokémon param is not available yet
        if (this.pokemon == null) {
            return super.getActiveEyeHeight(pose, dimensions)
        }
        return this.pokemon.form.eyeHeight(this)
    }

    fun setBehaviourFlag(flag: PokemonBehaviourFlag, on: Boolean) {
        behaviourFlags.set(setBitForByte(behaviourFlags.get(), flag.bit, on))
    }

    fun getBehaviourFlag(flag: PokemonBehaviourFlag): Boolean = getBitForByte(behaviourFlags.get(), flag.bit)

    @Suppress("UNUSED_PARAMETER")
    fun canBattle(player: PlayerEntity): Boolean {
        if (unbattleable.get()) {
            return false
        } else if (isBusy) {
            return false
        } else if (battleId.get().isPresent) {
            return false
        } else if (ownerUuid != null) {
            return false
        } else if (health <= 0F || isDead) {
            return false
        }

        return true
    }

    /**
     * The level this entity should display.
     *
     * @return The level that should be displayed, if equal or lesser than 0 the level is not intended to be displayed.
     */
    fun labelLevel() = this.labelLevel.get()

    override fun playAmbientSound() {
        if (!this.isSilent || this.busyLocks.filterIsInstance<EmptyPokeBallEntity>().isEmpty()) {
            val sound = Identifier(this.pokemon.species.resourceIdentifier.namespace, "pokemon.${this.pokemon.showdownId()}.ambient")
            // ToDo distance to travel is currently hardcoded to default we can maybe find a way to work around this down the line
            UnvalidatedPlaySoundS2CPacket(sound, this.soundCategory, this.x, this.y, this.z, this.soundVolume, this.soundPitch)
                .sendToPlayersAround(this.x, this.y, this.z, 16.0, this.world.registryKey)
        }
    }

    // We never want to allow an actual sound event here, we do not register our sounds to the sound registry as species are loaded by the time the registry is frozen.
    // Super call would do the same but might as well future-proof.
    override fun getAmbientSound() = null
    override fun getMinAmbientSoundDelay() = Cobblemon.config.ambientPokemonCryTicks

    private fun attemptItemInteraction(player: PlayerEntity, stack: ItemStack): Boolean {
        if (stack.isEmpty) {
            return false
        }

        if (player is ServerPlayerEntity && isBattling) {
            val battle = battleId.get().orElse(null)?.let(BattleRegistry::getBattle) ?: return false

            val bagItemConvertible = BagItems.getConvertibleForStack(stack) ?: return false

            val battlePokemon = battle.actors.flatMap { it.pokemonList }.find { it.effectedPokemon.uuid == pokemon.uuid } ?: return false // Shouldn't be possible but anyway
            if (battlePokemon.actor.getSide().actors.none { it.isForPlayer(player)}) {
                return true
            }

            return bagItemConvertible.handleInteraction(player, battlePokemon, stack)
        }
        if (player !is ServerPlayerEntity || this.isBusy) {
            return false
        }

        // Check evolution item interaction
        if (pokemon.getOwnerPlayer() == player) {
            val context = ItemInteractionEvolution.ItemInteractionContext(stack, player.world)
            pokemon.evolutions
                .filterIsInstance<ItemInteractionEvolution>()
                .forEach { evolution ->
                    if (evolution.attemptEvolution(pokemon, context)) {
                        if (!player.isCreative) {
                            stack.decrement(1)
                        }
                        this.world.playSoundServer(position = this.pos, sound = CobblemonSounds.ITEM_USE, volume = 1F, pitch = 1F)
                        return true
                    }
                }
        }

        (stack.item as? PokemonEntityInteraction)?.let {
            if (it.onInteraction(player, this, stack)) {
                it.sound?.let {
                    this.world.playSoundServer(
                        position = this.pos,
                        sound = it,
                        volume = 1F,
                        pitch = 1F
                    )
                }
                return true
            }
        }
        return false
    }

    fun offerHeldItem(player: PlayerEntity, stack: ItemStack): Boolean {
        if (player !is ServerPlayerEntity || this.isBusy || this.pokemon.getOwnerPlayer() != player) {
            return false
        }
        // We want the count of 1 in order to match the ItemStack#areEqual
        val giving = stack.copy().apply { count = 1 }
        val possibleReturn = this.pokemon.heldItemNoCopy()
        if (stack.isEmpty && possibleReturn.isEmpty) {
            return false
        }
        if (ItemStack.areEqual(giving, possibleReturn)) {
            player.sendMessage(lang("held_item.already_holding", this.pokemon.getDisplayName(), stack.name))
            return true
        }
        val returned = this.pokemon.swapHeldItem(stack, !player.isCreative)
        val text = when {
            giving.isEmpty -> lang("held_item.take", returned.name, this.pokemon.getDisplayName())
            returned.isEmpty -> lang("held_item.give", this.pokemon.getDisplayName(), giving.name)
            else -> lang("held_item.replace", returned.name, this.pokemon.getDisplayName(), giving.name)
        }
        player.giveOrDropItemStack(returned)
        player.sendMessage(text)
        this.world.playSoundServer(position = this.pos, sound = SoundEvents.ENTITY_ITEM_PICKUP, volume = 0.6F, pitch = 1.4F)
        return true
    }

    fun tryMountingShoulder(player: ServerPlayerEntity): Boolean {
        if (this.pokemon.belongsTo(player) && this.hasRoomToMount(player)) {
            CobblemonEvents.SHOULDER_MOUNT.postThen(ShoulderMountEvent(player, pokemon, isLeft = player.shoulderEntityLeft.isEmpty)) {
                val dirToPlayer = player.eyePos.subtract(pos).multiply(1.0, 0.0, 1.0).normalize()
                velocity = dirToPlayer.multiply(0.8).add(0.0, 0.5, 0.0)
                val lock = Any()
                busyLocks.add(lock)
                afterOnMain(seconds = 0.5F) {
                    busyLocks.remove(lock)
                    if (!isBusy && isAlive) {
                        val isLeft = player.shoulderEntityLeft.isEmpty
                        if (isLeft || player.shoulderEntityRight.isEmpty) {
                            pokemon.state = ShoulderedState(player.uuid, isLeft, pokemon.uuid)
                            this.mountOnto(player)
                            this.pokemon.form.shoulderEffects.forEach { it.applyEffect(this.pokemon, player, isLeft) }
                            this.world.playSoundServer(position = this.pos, sound = SoundEvents.ENTITY_ITEM_PICKUP, volume = 0.7F, pitch = 1.4F)
                            discard()
                        }
                    }
                }
                return true
            }
        }
        return false
    }

    override fun mountOnto(player: ServerPlayerEntity): Boolean {
        if (!super.mountOnto(player)) {
            return false
        }
        val nbt = when {
            player.shoulderEntityRight.isPokemonEntity() && player.shoulderEntityRight.getCompound(DataKeys.POKEMON).getUuid(DataKeys.POKEMON_UUID) == this.pokemon.uuid -> player.shoulderEntityRight
            player.shoulderEntityLeft.isPokemonEntity() && player.shoulderEntityLeft.getCompound(DataKeys.POKEMON).getUuid(DataKeys.POKEMON_UUID) == this.pokemon.uuid -> player.shoulderEntityLeft
            else -> return true
        }
        nbt.putUuid(DataKeys.SHOULDER_UUID, this.pokemon.uuid)
        nbt.putString(DataKeys.SHOULDER_SPECIES, this.pokemon.species.resourceIdentifier.toString())
        nbt.putString(DataKeys.SHOULDER_FORM, this.pokemon.form.name)
        nbt.put(DataKeys.SHOULDER_ASPECTS, this.pokemon.aspects.map(NbtString::of).toNbtList())
        nbt.putFloat(DataKeys.SHOULDER_SCALE_MODIFIER, this.pokemon.scaleModifier)
        return true
    }

    override fun remove(reason: RemovalReason) {
        val stateEntity = (pokemon.state as? ActivePokemonState)?.entity
        super.remove(reason)
        if (stateEntity == this) {
            pokemon.state = InactivePokemonState()
        }
        subscriptions.forEach(ObservableSubscription<*>::unsubscribe)
        removalObservable.emit(reason)

        if (reason.shouldDestroy() && pokemon.tetheringId != null) {
            pokemon.tetheringId = null
        }
    }

    // Copy and paste of how vanilla checks it, unfortunately no util method you can only add then wait for the result
    fun hasRoomToMount(player: PlayerEntity): Boolean {
        return (player.shoulderEntityLeft.isEmpty || player.shoulderEntityRight.isEmpty)
                && !player.hasVehicle()
                && player.isOnGround
                && !player.isTouchingWater
                && !player.inPowderSnow
    }

    fun cry() {
        val pkt = PokemonCryPacket(id)
        world.getEntitiesByClass(ServerPlayerEntity::class.java, Box.of(pos, 64.0, 64.0, 64.0), { true }).forEach {
            it.sendPacket(pkt)
        }
    }

    override fun drop(source: DamageSource?) {
        if (pokemon.isWild()) {
            super.drop(source)
            delegate.drop(source)
        }
    }

    override fun dropXp() {
        // Copied over the entire function because it's the simplest way to switch out the gamerule check
        if (
            world is ServerWorld && !this.isExperienceDroppingDisabled &&
            (shouldAlwaysDropXp() || playerHitTimer > 0 && shouldDropXp() && world.gameRules.getBoolean(CobblemonGameRules.DO_POKEMON_LOOT))
        ) {
            ExperienceOrbEntity.spawn(world as ServerWorld, pos, this.xpToDrop)
        }
    }

    override fun updatePostDeath() {
        // Do not invoke super we need to keep a tight lid on this due to the Thorium mod forcing the ticks to a max of 20 on server side if we invoke a field update here
        // Client delegate is mimicking expected behavior on client end.
        delegate.updatePostDeath()
    }

    override fun onEatingGrass() {
        super.onEatingGrass()

        val feature = pokemon.getFeature<FlagSpeciesFeature>(DataKeys.HAS_BEEN_SHEARED)
        if (feature != null) {
            feature.enabled = false
            pokemon.markFeatureDirty(feature)
            pokemon.updateAspects()
        }
    }

    override fun travel(movementInput: Vec3d) {
        val prevBlockPos = this.blockPos
        super.travel(movementInput)
        this.updateBlocksTraveled(prevBlockPos)
    }

    private fun updateBlocksTraveled(fromBp: BlockPos) {
        // Riding or falling shouldn't count, other movement sources are fine
        if (this.hasVehicle() || this.isFalling()) {
            return
        }
        val blocksTaken = this.blockPos.getSquaredDistance(fromBp)
        if (blocksTaken > 0) this.blocksTraveled += blocksTaken
    }

    private fun updateEyeHeight() {
        @Suppress("CAST_NEVER_SUCCEEDS")
        (this as com.cobblemon.mod.common.mixin.accessor.AccessorEntity).standingEyeHeight(this.getActiveEyeHeight(EntityPose.STANDING, this.type.dimensions))
    }

    fun isFlying() = this.getBehaviourFlag(PokemonBehaviourFlag.FLYING)
    fun couldStopFlying() = isFlying() && !behaviour.moving.walk.avoidsLand && behaviour.moving.walk.canWalk
    fun isFalling() = this.fallDistance > 0 && this.world.getBlockState(this.blockPos.down()).isAir && !this.isFlying()
    fun getIsSubmerged() = isInLava || isSubmergedInWater
    override fun getPoseType(): PoseType = this.poseType.get()

    /**
     * Returns the [Species.translatedName] of the backing [pokemon].
     *
     * @return The [Species.translatedName] of the backing [pokemon].
     */
    override fun getDefaultName(): Text = this.pokemon.species.translatedName

    /**
     * If this Pokémon has a nickname, then the nickname is returned.
     * Otherwise, [getDefaultName] is returned
     *
     * @return The current display name of this entity.
     */
    override fun getName(): Text {
        if (!nicknameVisible.get()) return defaultName
        return nickname.get()?.takeIf { it.content != TextContent.EMPTY } ?: pokemon.getDisplayName()
    }
    //Text.literal("atk:${pokemon.attack} def:${pokemon.defence} spatk:${pokemon.specialAttack} spdef:${pokemon.specialDefence} spd:${pokemon.speed}")
    /**
     * Returns the custom name of this entity, in the context of Cobblemon it is the [Pokemon.nickname].
     *
     * @return The nickname of the backing [pokemon].
     */
    override fun getCustomName(): Text? = pokemon.nickname

    /**
     * Sets the custom name of this entity.
     * In the context of a Pokémon entity this affects the [Pokemon.nickname].
     *
     * @param name The new name being set, if null the [Pokemon.nickname] is removed.
     */
    override fun setCustomName(name: Text?) {
        // We do this as a compromise to keep as much compatibility as possible with other mods expecting this entity to act like a vanilla one
        this.pokemon.nickname = Text.literal(name?.string)
    }

    /**
     * Checks if the backing [pokemon] has a non-null [Pokemon.nickname].
     *
     * @return If the backing [pokemon] has a non-null [Pokemon.nickname].
     */
    override fun hasCustomName(): Boolean = pokemon.nickname != null && pokemon.nickname?.content != TextContent.EMPTY

    /**
     * This method toggles the visibility of the entity name,
     * Unlike the vanilla implementation in our context it changes between displaying the species name or nickname of the Pokémon.
     *
     * @param visible The state of custom name visibility.
     */
    override fun setCustomNameVisible(visible: Boolean) {
        // We do this as a compromise to keep as much compatibility as possible with other mods expecting this entity to act like a vanilla one
        nicknameVisible.set(visible)
    }

    /**
     * In the context of a Pokémon entity this checks if the Pokémon is currently set to displaying its nickname.
     *
     * @return If the custom name of this entity should display, in this case the [getCustomName] is the nickname but if null the [getDefaultName] will be used.
     */
    override fun isCustomNameVisible(): Boolean = nicknameVisible.get()

    /**
     * Returns whether the entity is currently set to having its name displayed.
     *
     * @return If this entity should render the name label.
     */
    override fun shouldRenderName(): Boolean = shouldRenderName.get()

    /**
     * Sets the entity to having its name hidden.
     */
    fun hideNameRendering() { shouldRenderName.set(false) }

    override fun isBreedingItem(stack: ItemStack): Boolean = false

    override fun canBreedWith(other: AnimalEntity): Boolean = false

    override fun breed(world: ServerWorld, other: AnimalEntity) {}

    override fun method_48926(): EntityView {
        return this.world
    }

    override fun sheared(shearedSoundCategory: SoundCategory) {
        this.world.playSoundFromEntity(null, this,SoundEvents.ENTITY_SHEEP_SHEAR, shearedSoundCategory, 1.0F, 1.0F)
        val feature = this.pokemon.getFeature<FlagSpeciesFeature>(DataKeys.HAS_BEEN_SHEARED) ?: return
        feature.enabled = true
        this.pokemon.markFeatureDirty(feature)
        this.pokemon.updateAspects()
        val i = this.random.nextInt(3) + 1
        for (j in 0 until i) {
            val itemEntity = this.dropItem(Items.WHITE_WOOL, 1) ?: return
            itemEntity.velocity = itemEntity.velocity.add(
                ((this.random.nextFloat() - this.random.nextFloat()) * 0.1f).toDouble(),
                (this.random.nextFloat() * 0.05f).toDouble(),
                ((this.random.nextFloat() - this.random.nextFloat()) * 0.1f).toDouble()
            )
        }
    }

    override fun isShearable(): Boolean {
        val feature = this.pokemon.getFeature<FlagSpeciesFeature>(DataKeys.HAS_BEEN_SHEARED) ?: return false
        return !this.isBusy && !this.pokemon.isFainted() && !feature.enabled
    }

    override fun canUsePortals() = true

    override fun setAir(air: Int) {
        if (this.isBattling) {
            this.dataTracker.set(AIR, 300)
            return
        }
        super.setAir(air)
    }

    override fun onStoppedTrackingBy(player: ServerPlayerEntity?) {
        if (player == null) {
            return
        }

        if(this.ownerUuid == player.uuid && tethering == null) {
            if (player.isDisconnected) {
                this.remove(RemovalReason.DISCARDED)
                return
            }

            val chunkPos = ChunkPos(BlockPos(x.toInt(), y.toInt(), z.toInt()))
            (world as ServerWorld).chunkManager
                .addTicket(ChunkTicketType.POST_TELEPORT, chunkPos, 0, id)
            this.goalSelector.tick()
            if(distanceTo(player.blockPos) > 100) pokemon.recall()
        }
    }

    override fun canBeLeashedBy(player: PlayerEntity): Boolean {
        return this.ownerUuid == null || this.ownerUuid == player.uuid
    }

    fun setFireActive(fireActive: Boolean) {
        var b = BLAZE_FLAGS
        b = if (fireActive) {
            (b.toInt() or 1).toByte()
        } else {
            (b.toInt() and -2).toByte()
        }
        BLAZE_FLAGS= b
    }
}
